# No 1
![Demo Image to Text Character Recognition LearnId](https://gitlab.com/learnid/documentation/-/raw/main/ScanToTranslate.gif)

* https://github.com/fikri-taufiqurrahman/LearnId-ReactJS-Services.git
* https://github.com/fikri-taufiqurrahman/LearnId-Flask-Services.git
* https://github.com/fikri-taufiqurrahman/LearnId-NodeJS-Services.git

* https://youtu.be/w0prb7RM9a8



# No 2
Aplikasi web yang saya buat berfokus pada pembelajaran bahasa inggris dengan fitur-fitur seperti scan to translate, translate with speech recognition dan quiz.Dalam aplikasi LearnId, terdapat fitur-fitur yang tersedia yaitu:

## image compression
Fitur image compression digunakan setelah user mengupload gambar yang selanjutnya akan di process untuk fitur setelahnya yaitu image-to-text Optical Character Recognition(OCR), Compresi ini menggunakkan teknologi atau library dari openCV. OpenCV menggunakan algoritma kompresi JPEG (Joint Photographic Experts Group) untuk melakukan kompresi gambar dalam contoh kode yang Anda berikan. Algoritma kompresi JPEG adalah salah satu algoritma kompresi gambar yang umum digunakan untuk mengurangi ukuran file gambar dengan mempertahankan kualitas visual yang baik. Algoritma kompresi JPEG bekerja dengan cara memanfaatkan sifat-sifat dari gambar-gambar fotografi atau citra berwarna, di mana sebagian besar informasi visualnya terkandung dalam komponen frekuensi rendah (low-frequency) dibandingkan komponen frekuensi tinggi (high-frequency). Algoritma ini memanfaatkan transformasi Discrete Cosine Transform (DCT) untuk mengubah citra dari domain spasial menjadi domain frekuensi, kemudian melakukan kompresi dengan mempertahankan komponen frekuensi yang signifikan dan menghapus komponen frekuensi yang kurang signifikan. Dengan menggunakan algoritma kompresi JPEG, OpenCV dapat menghasilkan file gambar dengan ukuran yang lebih kecil dibandingkan dengan file gambar aslinya, sambil mempertahankan kualitas visual yang dapat diterima secara subjektif.

## image-to-text (character recognition):
Cara kerja fiturnya adalah menggunakan library Pytesseract. PyTesseract OCR adalah perangkat lunak sumber terbuka yang dikembangkan oleh Google dan saat ini dikelola oleh komunitas open-source. Algoritma utama yang digunakan oleh Tesseract OCR adalah algoritma Optical Character Recognition (OCR). Tesseract OCR menggunakan pendekatan yang berbasis pada pemrosesan gambar, segmentasi karakter, ekstraksi fitur, dan klasifikasi karakter untuk mengenali dan mengekstraksi teks dari gambar. Pytesseract sebagai wrapper Python menyediakan antarmuka yang memungkinkan pengguna untuk menggunakan Tesseract OCR dalam kode Python dengan lebih mudah.

# No 3
## OCR
OCR adalah kependekan dari Optical Character Recognition (Pengenalan Karakter Optik). Ini adalah teknologi yang digunakan untuk mengenali dan mengekstraksi teks dari gambar atau dokumen yang sudah ada. OCR berfungsi dengan mengubah teks yang tercetak atau ditulis tangan menjadi teks yang dapat diolah komputer.
Proses OCR dimulai dengan mengambil gambar atau memindai dokumen yang berisi teks. Kemudian, perangkat lunak OCR menganalisis gambar dan mencoba mengenali pola dan bentuk karakter yang ada di dalamnya. Setelah karakter-karakter dikenali, teks tersebut dapat diterjemahkan ke dalam format yang dapat diubah atau diedit, seperti teks yang dapat disalin dan ditempelkan ke dalam dokumen pengolah kata.


Tesseract OCR (Optical Character Recognition) adalah perangkat lunak sumber terbuka yang dikembangkan oleh Google untuk mengenali dan mengekstraksi teks dari gambar. Algoritma-algoritma utama yang digunakan oleh Tesseract OCR meliputi:

1. Preprocessing: Tesseract OCR menggunakan berbagai teknik preprocessing untuk mempersiapkan gambar sebelum proses pengenalan karakter. Ini meliputi pemotongan (cropping) gambar, penyesuaian kontras dan kecerahan, serta penghapusan noise dan gangguan lainnya agar teks lebih jelas dan terlihat dengan baik.

2. Binarization: Tahap binarization melibatkan mengubah gambar ke dalam format biner (hitam putih) dengan memisahkan teks dari latar belakangnya. Algoritma seperti Otsu's thresholding atau algoritma adaptif dapat digunakan untuk mencapai ini.

3. Segmentasi karakter: Setelah gambar biner dihasilkan, langkah berikutnya adalah memisahkan karakter-karakter individual dalam gambar. Algoritma pemisahan karakter seperti Connected Component Analysis (CCA) atau algoritma region growing dapat digunakan untuk mengidentifikasi dan memisahkan karakter satu per satu.

4. Pengenalan karakter: Tahap pengenalan karakter adalah inti dari Tesseract OCR. Algoritma yang digunakan di sini termasuk menggunakan model statistik dan machine learning untuk membandingkan pola piksel karakter yang tersegmentasi dengan data pelatihan. Tesseract OCR menggunakan metode rekognisi karakter berbasis kamus (dictionary-based) dan pemodelan statistik seperti Hidden Markov Models (HMM) untuk mengenali karakter dalam gambar.

5. Post-processing: Setelah tahap pengenalan karakter, Tesseract OCR dapat menerapkan berbagai metode post-processing untuk meningkatkan hasil akhirnya. Ini termasuk pengenalan bahasa (language modeling), pemrosesan konteks, dan pembersihan teks untuk menghilangkan kesalahan dan meningkatkan akurasi.


Flowchart
<div class="center">

```mermaid
flowchart TD;
    A((Start)) --> B[Load Image];
    B --> |Image Loaded| C[Convert Image to RGB];
    C --> D[Apply Color Space Conversion];
    D --> E[Perform Image Segmentation];
    E --> F[Apply Lossless Compression];
    F --> G[Save Compressed Image];
    G --> H((End));
    B --> |Image Not Loaded| I[Handle Error];
    I --> H;
    F --> |Image Intact| J[Perform OCR];
    J --> K[Extract Text];
    K --> L[Save Text];
    L --> M[Perform Translation];
    M --> N[Save Translated Text];
    N --> H;
    F --> |Image Altered| H;
```
<div>


# No 4
![Demo Aplikasi Learn Id Text-to-speech, Audio Processing (slow mode), speech recognition](https://gitlab.com/learnid/documentation/-/raw/main/AudioProcessingSpeechRecognitionTexttoSpeechAudioCompression.gif)

* https://github.com/fikri-taufiqurrahman/LearnId-ReactJS-Services.git
* https://github.com/fikri-taufiqurrahman/LearnId-Flask-Services.git
* https://github.com/fikri-taufiqurrahman/LearnId-NodeJS-Services.git

* https://youtu.be/w0prb7RM9a8

# No 5

## slow audio (audio processing):
Slow audio disini adalah untuk memudahkan pengguna untuk mengenali suara atau pengucapan dalam bahasa yang ia ingin pelajari. ini menggunakan ffmpeg dengan faktor kecepatan 80% dari kecepatan asli



## audio compresion
Audio compression adalah proses mengurangi ukuran file audio tanpa mengorbankan kualitas audio yang signifikan. Aplikasi ini menggunakan library ffmpeg untuk audio compression. FFMPEG adalah sebuah perangkat lunak baris perintah yang memiliki fitur kompresi audio yang kuat. 

## translate:
cara kerja fiturnya adalah menggunakan library googletrans. dengan menginput text yang akan di translate serta tujuan atau destination bahasa untuk outputnya. 



# No 6
## speech-recognition
Speech recognition, atau pengenalan suara, adalah teknologi yang digunakan untuk menerjemahkan ucapan manusia menjadi teks tertulis. Ini melibatkan kemampuan komputer untuk mendeteksi dan mengenali kata-kata dan frasa yang diucapkan oleh seseorang, kemudian mengubahnya menjadi teks yang dapat dipahami dan diproses oleh komputer.

Proses speech recognition dimulai dengan perekaman suara menggunakan mikrofon. Suara yang direkam kemudian dianalisis oleh perangkat lunak speech recognition yang menggunakan algoritma dan model linguistik untuk mengenali kata-kata yang diucapkan. Selama proses ini, fitur-fitur suara seperti frekuensi, durasi, intonasi, dan pola bunyi digunakan untuk mengidentifikasi dan memahami ucapan.

Setelah kata-kata dikenali, mereka dikonversi menjadi teks tertulis. Hasil teks ini dapat digunakan untuk berbagai tujuan, seperti pengenalan perintah suara untuk mengendalikan perangkat, transkripsi wawancara atau percakapan, pemahaman percakapan pada sistem interaksi manusia-komputer, dan banyak lagi.

Google menggunakan berbagai algoritma dan teknik dalam sistem pengenalan ucapan (speech recognition) mereka. Beberapa algoritma yang mungkin digunakan oleh Google dalam pengenalan ucapan termasuk:

1. Hidden Markov Models (HMM): HMM adalah model statistik yang umum digunakan dalam pengenalan ucapan. Algoritma ini memodelkan suara dengan urutan kecil unit suara yang disebut fonem. HMM mempelajari pola-pola fonem dari data pelatihan dan kemudian menggunakannya untuk memprediksi fonem yang mungkin terdengar dalam ucapan.

2. Deep Neural Networks (DNN): DNN merupakan jenis jaringan saraf tiruan yang digunakan dalam pengenalan ucapan oleh Google. DNN dapat mempelajari pola-pola kompleks dalam data pelatihan dan mampu mengekstraksi fitur-fitur yang relevan untuk pengenalan ucapan.

3. Recurrent Neural Networks (RNN): RNN adalah jenis jaringan saraf yang memungkinkan pemodelan urutan data, yang penting dalam pengenalan ucapan. RNN dapat mempelajari hubungan antara konteks sebelumnya dan konteks saat ini dalam rangkaian suara, membantu dalam pemahaman dan pemrosesan ucapan yang berkelanjutan.

4. Connectionist Temporal Classification (CTC): CTC adalah algoritma yang digunakan untuk pelabelan urutan waktu yang tidak terawasi. Dalam pengenalan ucapan, CTC dapat digunakan untuk menyesuaikan model untuk menghasilkan urutan fonem yang paling mungkin berdasarkan ucapan yang diterima.

5. Deep Reinforcement Learning: Google juga telah menggunakan teknik deep reinforcement learning dalam pengenalan ucapan. Pendekatan ini melibatkan penggunaan model jaringan saraf yang dilatih menggunakan metode pembelajaran penguatan untuk meningkatkan performa pengenalan ucapan seiring berjalannya waktu dan pengalaman pengguna.

<div class="center">


```mermaid
flowchart TD;
A((Start)) --> B(Record Audio);
B --> C{Audio Quality Check};
C -- Good Quality --> D{Background Noise Check};
D -- No Noise --> E(Speech Recognition);
E -- Successful --> F[Speech Recognized];
D -- Background Noise --> G[Background Noise];
C -- Poor Quality --> H[Poor Quality];
E -- Error --> I[Handle Error];
G --> I;
H --> I;
F --> J;
I --> J;
J((End));
```
</div>

## text-to-speech


Text to speech (TTS), atau pengubah teks ke suara, adalah teknologi yang mengubah teks tertulis menjadi suara yang dapat didengar. Ini memungkinkan komputer untuk membaca teks dengan suara yang manusiawi. Teknologi TTS menggunakan sintesis suara untuk menghasilkan ucapan yang dapat dipahami oleh pendengar.

Proses TTS dimulai dengan memasukkan teks tertulis ke dalam perangkat lunak atau sistem TTS. Kemudian, sistem menganalisis teks tersebut, menguraikannya menjadi unit-unit suara yang lebih kecil (seperti fonem, suku kata, atau frasa) dan menghasilkan suara yang sesuai dengan teks tersebut. Ada berbagai teknik yang digunakan dalam sintesis suara, termasuk sintesis berbasis aturan, sintesis berbasis konkatenasi, dan sintesis berbasis statistik.


Google menggunakan beberapa algoritma dan teknik dalam teknologi Text-to-Speech (TTS) mereka. Berikut adalah beberapa algoritma yang mungkin digunakan oleh Google dalam TTS:

1. Unit Selection Synthesis: Algoritma ini melibatkan pemilihan dan penggabungan unit-unit suara yang telah direkam sebelumnya untuk menghasilkan suara yang diinginkan. Unit-unit suara ini bisa berupa fonem, suku kata, atau frasa. Algoritma ini berusaha untuk mencocokkan intonasi dan karakteristik suara yang paling sesuai dengan teks yang diberikan.

2. Hidden Markov Models (HMM): HMM juga dapat digunakan dalam TTS untuk menghasilkan suara. Dalam hal ini, HMM digunakan untuk memodelkan dan mensintesis unit-unit suara berdasarkan teks yang diberikan. Setiap unit suara diperlakukan sebagai "state" dalam HMM, dan transisi antara unit-unit suara ini ditentukan oleh model probabilitas.

3. Deep Neural Networks (DNN): DNN telah digunakan secara luas dalam TTS. Dalam hal ini, jaringan saraf tiruan dalam bentuk DNN digunakan untuk mempelajari korelasi antara teks yang diberikan dan suara yang dihasilkan. DNN dapat mengubah teks menjadi representasi numerik yang kemudian diubah menjadi suara melalui sintesis vokal.

4. WaveNet: WaveNet adalah model generatif yang digunakan oleh Google dalam TTS. Model ini menggunakan jaringan saraf generatif yang sangat dalam untuk mensintesis suara. WaveNet dapat menghasilkan suara yang sangat realistis dan mendekati kualitas suara manusia dengan menggunakan sampel suara yang sangat kecil sebagai input.

5. Tacotron: Tacotron adalah model jaringan saraf yang dikembangkan oleh Google yang menghasilkan suara manusia dari teks yang diberikan. Model ini menggabungkan teknik deep learning dan sintesis vokal untuk menghasilkan suara yang alami dan nyata.

<div class="center">

```mermaid
flowchart TD;
A((Start)) --> B{Text Input};
B -- Text processing --> C{Valid Text?};
C -- Yes --> D[Convert Text to Speech];
C -- No --> E[Display Error Message];
D -- Audio Output --> F((End));
E --> F;
```

</div>
